<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *      normalizationContext={
 *                  "groups"={"albums_read",
 *                   "tracks_read"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 *  * @Vich\Uploadable
 */
// *     collectionOperations={"get"},
// *     itemOperations={"get"}

class Album
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"albums_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"albums_read", "tracks_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     * @Groups({"albums_read"})
     */
    private $year_release;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Artist", inversedBy="albums")
     * @Groups({"albums_read"})
     */
    private $artist;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"albums_read"})
     */
    private $genre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"albums_read"})
     */
    private $cover;

    /**
     * @Vich\UploadableField(mapping="album_cover", fileNameProperty="cover")
     */
    private $album_cover;

    public function getAlbumCover(): ?File
    {
        return $this->album_cover;
    }


    public function setAlbumCover(File $album_cover = null): self
    {
        if ($album_cover) {

            $this->album_cover = $album_cover;
        }

        return $this;
    }

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Track", mappedBy="album")
     */
    private $tracks;

    public function __construct()
    {
        $this->tracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYearRelease(): ?\DateTimeInterface
    {
        return $this->year_release;
    }

    public function setYearRelease(\DateTimeInterface $year_release): self
    {
        $this->year_release = $year_release;

        return $this;
    }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return Collection|Track[]
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track)) {
            $this->tracks[] = $track;
            $track->addAlbum($this);
        }

        return $this;
    }

    public function removeTrack(Track $track): self
    {
        if ($this->tracks->contains($track)) {
            $this->tracks->removeElement($track);
            $track->removeAlbum($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
