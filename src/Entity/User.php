<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email", message="The email already exists.")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Email cannot be blank.")
     * @Assert\Email(message="Email address must have a valid format.")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Password cannot be blank.")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Firstame cannot be blank.")
     * @Assert\Length(min=3, minMessage="Firstname must have min 3 characters.", max=255, maxMessage="Firstname must have max 255 characters.")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Lastname cannot be blank.")
     * @Assert\Length(min=3, minMessage="Lastname must have min 3 characters.", max=255, maxMessage="Firstname must have max 255 characters.")
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Username cannot be blank.")
     * @Assert\Length(min=3, minMessage="Username must have min 3 characters.", max=20, maxMessage="Username must have max 20 characters.")
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="users")
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="followers")
     */
    private $users;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="Birthday cannot be blank.")
     */
    private $birthday;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayList", mappedBy="owner")
     */
    private $playLists;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Track", inversedBy="fans")
     */
    private $loved_tracks;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Artist", mappedBy="followed")
     */
    private $loved_artists;

    public function __construct()
    {
        $this->followers = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->playLists = new ArrayCollection();
        $this->loved_tracks = new ArrayCollection();
        $this->loved_artists = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    public function addFollower(self $follower): self
    {
        if (!$this->followers->contains($follower)) {
            $this->followers[] = $follower;
        }

        return $this;
    }

    public function removeFollower(self $follower): self
    {
        if ($this->followers->contains($follower)) {
            $this->followers->removeElement($follower);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(self $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addFollower($this);
        }

        return $this;
    }

    public function removeUser(self $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeFollower($this);
        }

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection|PlayList[]
     */
    public function getPlayLists(): Collection
    {
        return $this->playLists;
    }

    public function addPlayList(PlayList $playList): self
    {
        if (!$this->playLists->contains($playList)) {
            $this->playLists[] = $playList;
            $playList->setOwner($this);
        }

        return $this;
    }

    public function removePlayList(PlayList $playList): self
    {
        if ($this->playLists->contains($playList)) {
            $this->playLists->removeElement($playList);
            // set the owning side to null (unless already changed)
            if ($playList->getOwner() === $this) {
                $playList->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Track[]
     */
    public function getLovedTracks(): Collection
    {
        return $this->loved_tracks;
    }

    public function addLovedTrack(Track $lovedTrack): self
    {
        if (!$this->loved_tracks->contains($lovedTrack)) {
            $this->loved_tracks[] = $lovedTrack;
        }

        return $this;
    }

    public function removeLovedTrack(Track $lovedTrack): self
    {
        if ($this->loved_tracks->contains($lovedTrack)) {
            $this->loved_tracks->removeElement($lovedTrack);
        }

        return $this;
    }

    /**
     * @return Collection|Artist[]
     */
    public function getLovedArtists(): Collection
    {
        return $this->loved_artists;
    }

    public function addLovedArtist(Artist $lovedArtist): self
    {
        if (!$this->loved_artists->contains($lovedArtist)) {
            $this->loved_artists[] = $lovedArtist;
            $lovedArtist->addFollowed($this);
        }

        return $this;
    }

    public function removeLovedArtist(Artist $lovedArtist): self
    {
        if ($this->loved_artists->contains($lovedArtist)) {
            $this->loved_artists->removeElement($lovedArtist);
            $lovedArtist->removeFollowed($this);
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getName();
    }
}
