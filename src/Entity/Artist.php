<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ApiResource(
 *     attributes={"force_eager"=false,
 *                  "normalization_context"={
 *                         "groups"={"artists_read",
 *                          "tracks_read"}
 *                  }
 *           }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ArtistRepository")
 *  * @Vich\Uploadable
 */
// *     collectionOperations={"get"},
// *     itemOperations={"get"}),
class Artist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"artists_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"artists_read", "tracks_read"})
     * @MaxDepth(2)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"artists_read"})
     * @MaxDepth(2)
     */
    private $poster;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Album", mappedBy="artist")
     * @Groups({"artists_read"})
     * @MaxDepth(2)
     */
    private $albums;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="loved_artists")
     * @Groups({"artists_read"})
     * @MaxDepth(2)
     */
    private $followed;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Track", mappedBy="artist", orphanRemoval=true)
     * @Groups({"artists_read"})
     * @MaxDepth(2)
     */
    private $tracks;

    public function __construct()
    {
        $this->albums = new ArrayCollection();
        $this->followed = new ArrayCollection();
        $this->tracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return @Vich\UploadableField(mapping="artist_poster", fileNameProperty="poster")
     */
    private $artist_poster;

    public function getArtistPoster(): ?File
    {
        return $this->artist_poster;
    }

    public function setArtistPoster(File $artist_poster): self
    {
        $this->artist_poster = $artist_poster;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->poster;
    }

    public function setPoster(string $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setArtist($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getArtist() === $this) {
                $album->setArtist(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFollowed(): Collection
    {
        return $this->followed;
    }

    public function addFollowed(User $followed): self
    {
        if (!$this->followed->contains($followed)) {
            $this->followed[] = $followed;
        }

        return $this;
    }

    public function removeFollowed(User $followed): self
    {
        if ($this->followed->contains($followed)) {
            $this->followed->removeElement($followed);
        }

        return $this;
    }

    /**
     * @return Collection|Track[]
     */
    public function getTracks(): Collection
    {
        return $this->tracks;
    }

    public function addTrack(Track $track): self
    {
        if (!$this->tracks->contains($track)) {
            $this->tracks[] = $track;
            $track->setArtist($this);
        }

        return $this;
    }

    public function removeTrack(Track $track): self
    {
        if ($this->tracks->contains($track)) {
            $this->tracks->removeElement($track);
            // set the owning side to null (unless already changed)
            if ($track->getArtist() === $this) {
                $track->setArtist(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
