<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"tracks_read"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TrackRepository")
 * @Vich\Uploadable
 */
// collectionOperations={"get"}, itemOperations={"get"}),
class Track
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"tracks_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"tracks_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"tracks_read"})
     * @ApiProperty(readable=true)
     *
     */
    private $file_mp3;

    /**
     * @Vich\UploadableField(mapping="tracks", fileNameProperty="file_mp3")
     */
    private $file_mp3_file;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="time")
     * @Groups({"tracks_read"})
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"tracks_read"})
     */
    private $bpm;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", inversedBy="tracks")
     * @Groups({"tracks_read"})
     */
    private $album;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PlayList", mappedBy="song_name")
     */
    private $playLists;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="loved_tracks")
     */
    private $fans;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Artist", inversedBy="tracks")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"tracks_read"})
     */
    private $artist;

    public function __construct()
    {
        $this->album = new ArrayCollection();
        $this->playLists = new ArrayCollection();
        $this->fans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFileMp3(): ?string
    {
        return $this->file_mp3;
    }

    public function setFileMp3(?string $file_mp3): self
    {
        $this->file_mp3 = $file_mp3;

        return $this;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getBpm(): ?int
    {
        return $this->bpm;
    }

    public function setBpm(?int $bpm): self
    {
        $this->bpm = $bpm;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbum(): Collection
    {
        return $this->album;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->album->contains($album)) {
            $this->album[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->album->contains($album)) {
            $this->album->removeElement($album);
        }

        return $this;
    }

    /**
     * @return Collection|PlayList[]
     */
    public function getPlayLists(): Collection
    {
        return $this->playLists;
    }

    public function addPlayList(PlayList $playList): self
    {
        if (!$this->playLists->contains($playList)) {
            $this->playLists[] = $playList;
            $playList->addSongName($this);
        }

        return $this;
    }

    public function removePlayList(PlayList $playList): self
    {
        if ($this->playLists->contains($playList)) {
            $this->playLists->removeElement($playList);
            $playList->removeSongName($this);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getFans(): Collection
    {
        return $this->fans;
    }

    public function addFan(User $fan): self
    {
        if (!$this->fans->contains($fan)) {
            $this->fans[] = $fan;
            $fan->addLovedTrack($this);
        }

        return $this;
    }

    public function removeFan(User $fan): self
    {
        if ($this->fans->contains($fan)) {
            $this->fans->removeElement($fan);
            $fan->removeLovedTrack($this);
        }

        return $this;
    }

    public function getArtist(): ?Artist
    {
        return $this->artist;
    }

    public function setArtist(?Artist $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getFileMp3File(): ?File
    {
        return $this->file_mp3_file;
    }

    public function setFileMp3File(?File $file_mp3_file): self
    {
        $this->file_mp3_file = $file_mp3_file;

        if ($file_mp3_file instanceof File) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        if(empty($updatedAt)) {
            $this->updatedAt = new \DateTime();
        }
        else {
            $this->updatedAt = $updatedAt;
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getFileMp3();
    }
}
