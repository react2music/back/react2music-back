<?php

namespace App\DataFixtures;
use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
{
    $this->encoder = $encoder;
}
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create ('fr_FR');

        for ($u= 0; $u < 10; $u++ ) {
            $user = new User();

            $hash = $this->encoder->encodePassword($user, "password");

            $user->setName($faker->name)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword($hash)
                ->setRoles(array('ROLE_USER'))
                ->setPseudo($faker->userName)
                ->setBirthday($faker->dateTimeBetween('-30 years', '-25 years'))
                ->setPhoto("default_user_photo.jpg");


            $manager -> persist($user);

        }

        $manager->flush();
    }
}
