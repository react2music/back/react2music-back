<?php

namespace App\Repository;

use App\Entity\FavoriteTrack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FavoriteTrack|null find($id, $lockMode = null, $lockVersion = null)
 * @method FavoriteTrack|null findOneBy(array $criteria, array $orderBy = null)
 * @method FavoriteTrack[]    findAll()
 * @method FavoriteTrack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteTrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FavoriteTrack::class);
    }

    // /**
    //  * @return FavoriteTrack[] Returns an array of FavoriteTrack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FavoriteTrack
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
