<?php

namespace App\Events;

use App\Repository\PlayListRepository;
use App\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\PlayList;
use Doctrine\ORM\EntityManagerInterface;


class CreatePlaylistSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $playlistRepository;
    private $userRepository;
    private $entityManager;

    public function __construct(PlayListRepository $playlistRepository,
                                UserRepository $userRepository,
                                EntityManagerInterface $entityManager
    )
    {
        $this->playlistRepository = $playlistRepository;
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;

    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['createPlaylist', EventPriorities::POST_WRITE]
            //after writing into the db (POST_WRITE) the user, after the deserialization, create a new playlist for the user
            //this list will contained the songs user would like to play
        ];
    }
    public function createPlaylist(ViewEvent $event)
    {
        $user = $event->getControllerResult(); //may not be a user, but we will only use if it'a a user
        //dd($user);
        $method = $event->getRequest()->getMethod(); // getting the method, we want POST
        //only if the request is a POST that wants to create a new user
        if ($user instanceof User && $method === "POST") {
            //create a playlist for the user (it will be the list where they can add songs)
            $now = new \DateTime();
            //getting the id of the newly inserted user
            $userId = $this->userRepository->findOneBy([],['id' => 'desc']);
            //   dd($user);
            //adding a new playlist for the newly inserted user
            $playList = new PlayList();
            $playList->setOwner($user);
            $playList->setName("Queue");
            $playList->setCreatedAt($now);
            //  dd($playList);
            $this->entityManager->persist($playList);
            $this->entityManager->flush();
            //id 	owner_id 	name 	created_at
        }
    }
}